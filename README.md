# Gitlab CI CD for nrf5340 project

## Motivation:
 The Main purpose of this project is to build the nrf sdk based project using the Gitlab runner and get the compiled hex files out of it.

 This should provide nrf developers following advantages:

 - Check that the code also works in the remote machine
 - Get the latest hex or elf files from the remote systems
 - Provide easy to monitor project status


 ## Environment:

 - OS: UBUNTU 20.04
 - Board: nrf5340dk
 - nrf sdk: zephyr and west


 ## Steps:

 1. Get and set the default OS or Image for the gitlab runner:

```yml

default:
  image: ubuntu:20.04

```

2. Update the default OS or Image for the gitlab runner. Also set the user:

```yml

  apt update
  uname -a

```

3. Install the python3 ccache and ninja-build package:

```yml

  apt install --no-install-recommends ccache ninja-build
  apt install python3 -y
  python3 --version

```

4. Set up the python virtual environment and install pyelftool needed for .elf file generation:

```yml

 apt install python3.8-venv -y
 python3 -m venv ~/.venv
 source ~/.venv/bin/activate
 pip3 install west ninja pyelftools

```

5. Install wget,git and cmake to the latest version:

```yml

 apt install wget -y
 apt install git -y
 apt update -y
 export DEBIAN_FRONTEND=noninteractive
 apt install -y software-properties-common lsb-release
 apt clean all
 apt-get install gnupg -y
 wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null
 apt-add-repository "deb https://apt.kitware.com/ubuntu/ $(lsb_release -cs) main"
 apt update
 apt install kitware-archive-keyring
 rm /etc/apt/trusted.gpg.d/kitware.gpg
 apt update
 apt install cmake -y

```

6. Download and Install zephyr sdk:

```yml

 wget -q https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.16.4/zephyr-sdk-0.16.4_linux-x86_64_minimal.tar.xz
 tar xf zephyr-sdk-0.16.4_linux-x86_64_minimal.tar.xz 
 zephyr-sdk-0.16.4/setup.sh -c -t arm-zephyr-eabi

```

7. Clean the workspace and Initialize west and build the project:

```yml

 rm -rf .west
 rm -rf zephyr
 west init
 west update --narrow -o=--depth=1
 pip3 install -r zephyr/scripts/requirements.txt
 west zephyr-export
 west build --pristine --board $ZEPHYR_BOARD --no-sysbuild -- -DNCS_TOOLCHAIN_VERSION=NONE  DCONF_FILE=prj.conf

```

8. Finally create a zip file out of the build directory.

```yml

 tar -cJf $CI_PROJECT_DIR/zephyr_build.tar.xz build/zephyr

```

9. You can also setup and artifact with the desired file name:

```yml

 artifacts:
      name: "${CI_PROJECT_NAME}-${CI_JOB_NAME}_${CI_COMMIT_SHORT_SHA}"
      expire_in: 10 mins 
      paths:
      - zephyr_build.tar.xz


```

10. Similar procedure should work for building nrf project in the ubuntu terminal.


----------------------------------------------------------------

