/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>


#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>


int main(void)
{
	while (1)
	{
		printf("Hello World! %s\n", CONFIG_BOARD);
		float t = 0.045;
		printf("Printing float %f\n", t);
		k_msleep(1000);
		float tmp = 123.456;
		printk("Ffloating number: %3.2f\n", tmp);
	}



	return 0;
}
